let num1 = 0;
let operator = null;
let num2 = 0;

//Al clicar un operador, almazena lo que hay en el label en la varible del primer numero y vacia el label
function operatorClicked(operatorParam) {
    num1 = Number(document.getElementById('resultado').textContent);
    operator = operatorParam;
    setResult('');
}

//Al clicar el boton de =, mira que operacion tiene que hacer segun el operador, la calcula y printea en el label
function equals() {
    num2 = Number(document.getElementById('resultado').textContent);
    switch(operator) {
        case '+':
            setResult(num1+num2);
            break;
        case '-':
            setResult(num1-num2);
            break;
        case '*':
            setResult(num1*num2);
            break;
        case '/':
            setResult(num1/num2);
            break;
        case '%':
            setResult(num1/100 * num2);
            break;
        case 'elev':
            setResult(Math.pow(num1, num2));
            break;
    }
}

//Anade numeros al label segun vamos clicando
function numClicked(num) {
    updateLabel(num);
}

//Funcion que controla el . y anade este al numero del label; controla y realiza los CE y C
function specialClicked(action) {
    if(action == '.') {
        updateLabel('.');
    }
    if(action == 'CE') {
        document.getElementById('resultado').innerHTML = '';
    }
    if(action == 'C') {
        num1 = 0;
        num2 = 0;
        document.getElementById('resultado').innerHTML = '';
    }
}

//Funciones que reciben un texto o numero y lo printean en el label de resultado
function updateLabel(txt) {
    document.getElementById('resultado').innerHTML += txt;
}

function setResult(result) {
    document.getElementById('resultado').innerHTML = result;
}